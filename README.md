# Desafio

Este projeto visa um teste de consulta no site da OLX e a chamada de uma api criada com a ajuda do mocky.

Este projeto foi feito no RobotFramework e colocado no Docker. O mesmo está disponível no Gitlab.

Foi criado projeto mydockerimages para gerar a imagem do que é necessário para a execução do desafio.

Para rodar o teste, basta rodar o pipeline e o mesmo executará. Ao final, será gerado um html com o log da execução e poderá ser acessado através dos artefatos do Projeto.

Para rodar os testes localmente no docker:

- buildar a imagem: docker build -t robotolx_image ./resources
- rodar o comando: docker run --rm --name exec -v "$(pwd)\resources\suites:/opt/robotframework/tests" -v "$(pwd)\results:/opt/robotframework/results" robotolx_image:latest robot -d /opt/robotframework/results /opt/robotframework/tests

Para rodar os testes localmente, basta informar a URL do teste em questão:

robot -d ./results -v BROWSER:headlesschrome ./resources/suites/BuscaOLX.robot
robot -d ./results -v BROWSER:headlesschrome ./resources/suites/Mocky.robot

Caso, deseje rodar todos os testes do projeto, basta informar o comando abaixo:

robot -d ./results -v BROWSER:headlesschrome ./resources/suites/

O log do mesmo será gerado na pasta "results" do projeto.
