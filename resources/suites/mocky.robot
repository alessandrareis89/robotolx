*** Settings ***
Library     RequestsLibrary
Library     Collections
Library     JSONLibrary

*** Variable ***
${url}            http://www.mocky.io/v2
${statusCode}     200
${world}          world

*** Test Case ***
Realizar um GET na api do mocky
  Create Session        mocky      ${url}
  ${header}             Create Dictionary    content-type=application/json
  ${responseGET}        Get Request          mocky            5e4149682f0000500058339e
  Set Suite Variable    ${responseGET}
  Log                   ${responseGET.url}
  Log                   ${responseGET.status_code}
  Log                   ${responseGET.content}

Realizar as assertivas do retorno
  Should Be True      ${responseGET.status_code}==${statusCode}
  ${responseJson}     Convert String to JSON      ${responseGET.content}
  ${responseHello}    Set Variable                ${responseJson["hello"]}
  Should Be Equal     ${responseHello}            ${world}
