*** Settings ***
Library     SeleniumLibrary

*** Variable ***
${url_OLX}              https://www.olx.com.br/brasil
${estado}               RJ
${produto}              HYUNDAI HB20X STYLE 1.6
${browser}              chrome
${email}                teste.totvs.olx@gmail.com
${senha}                testeolx
${options}              add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox")

*** Test Case ***
Abrir navegador
  Open Browser     ${url_OLX}    ${browser}    options=${options}

Realizar login
  Wait Until Element Is Visible    //div/a[@href="https://www3.olx.com.br/account/form_login"][2]
  Click Link                       //div/a[@href="https://www3.olx.com.br/account/form_login"][2]
  Wait Until Element Is Visible    //div[@class="sc-iWadT kHKZhB"]
  Set Focus To Element             //div[@class="sc-iWadT kHKZhB"]
  Input Text                       //input[@type="email"]    ${email}
  Input Password                   //input[@type="text"]     ${senha}
  Click Button                     //button[@type="text"]
  Sleep    3

Selecionar estado
  Execute JavaScript               window.scrollTo(0,100)
  Set Focus To Element             //header[@class="sc-esExBO iUvHZP"]
  Click Link                       //a[@href="https://www.olx.com.br/brasil"]
  Wait Until Element Is Visible    //a[@href="https://rj.olx.com.br"]
  Click Link                       //a[@href="https://rj.olx.com.br"]
  Wait Until Element Is Visible    //li[@class="sc-19czgis-3 hAgqxO"]/h2[1]
  ${estadoTela}        Get Text    //li[@class="sc-19czgis-3 hAgqxO"]/h2[1]
  Should Be Equal                  ${estadoTela}    ${estado}

Selecionar categoria
  Click Link         //a[@href="https://rj.olx.com.br/autos-e-pecas"]

Informar palavra-chave e pesquisar
  Wait Until Element Is Visible                //input[@name="q"]
  Input Text        //input[@name="q"]         ${produto}
  Click Button      //button[@type="submit"]

Selecionar o primeiro registro
  Wait Until Element Is Visible       //li[@class="sc-1fcmfeb-2 ggOGTJ"][1]
  ${tituloPrimeiroResultado}          Get Text    //li[@class="sc-1fcmfeb-2 ggOGTJ"][1]/a/div/div/div/div/h2
  ${valorPrimeiroResultado}           Get Text    //li[@class="sc-1fcmfeb-2 ggOGTJ"][1]/a/div/div/div/DIV[2]/p
  Set Suite Variable                  ${tituloPrimeiroResultado}
  Set Suite Variable                  ${valorPrimeiroResultado}
  Click Element                       //li[@class="sc-1fcmfeb-2 ggOGTJ"][1]

Assertivas na tela do produto
  Switch WindoW   NEW
  Wait Until Page Contains Element    //div[@class="h3us20-5 jTuobL"]
  ${tituloResultadoSelecionado}       Get Text    //div[@class="h3us20-5 jTuobL"]
  ${valorResultadoSelecionado}        Get Text    //h2[@class="sc-bZQynM sc-1leoitd-0 caJVsD"]
  Should Be Equal                     ${tituloPrimeiroResultado}     ${tituloResultadoSelecionado}
  Should Be Equal                     ${valorPrimeiroResultado}      ${valorResultadoSelecionado}

Efetuar logout
  Wait Until Element Is Visible    //div[@class="sc-EHOje sc-bMVAic jReFnW sc-jTzLTM ivCaxb"]
  Click Element                    //div[@class="sc-EHOje sc-bMVAic jReFnW sc-jTzLTM ivCaxb"]
  Set Focus To Element             root
  Click Element                    //a[@href="https://www3.olx.com.br/account/do_logout"]
